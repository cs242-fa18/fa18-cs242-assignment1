package view.graphGUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.SwingUtilities;

import controller.Game;
import model.chessBoard.Board;
import model.chessPieces.ChessPiece;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class GUI {

    private JFrame gameFrame;
    private BoardPanel boardPanel;

    private static Dimension WINDOW_DIMENSION = new Dimension(800, 600);
    private static Dimension BOARD_DIMENSION = new Dimension(600, 600);
    private static Dimension USER_BOARD_DIMENSION = new Dimension(200, 600);
    private static Dimension TILE_DIMENSION = new Dimension(10, 10);
    private String BASE_PATH = "/Users/Dorothy/Documents/CS242FA18/HW1.3/src/chessImages/";

    private Color light_tile_color = Color.decode("#FDEBD0");
    private Color dark_tile_color = Color.decode("#CC9966");

    private ChessPiece sourceTile;

    public Board board;
    public static ArrayList<ChessPiece> historyW; //White player history
    public static ArrayList<ChessPiece> historyB; //Black player history
    public static boolean endTurn;


    /**
     * This is the canvas for a chess board.
     */
    public GUI(){

        this.board = new Board();
        endTurn = false;

        this.gameFrame = new JFrame("Chess Board");
        JMenuBar tableMenuBar = new JMenuBar();
        this.populateMenuBar(tableMenuBar);
        this.gameFrame.setJMenuBar(tableMenuBar);
        this.gameFrame.setSize(WINDOW_DIMENSION);
        this.gameFrame.setLayout(new BorderLayout());

        this.boardPanel = new BoardPanel(board);
        this.gameFrame.add(this.boardPanel, BorderLayout.WEST);


        this.gameFrame.setVisible(true);

    }


    private void populateMenuBar(JMenuBar tableMenuBar) {
        tableMenuBar.add(createGameMenu());
        tableMenuBar.add(createOperationMenu());
        tableMenuBar.add(createScoreMenu());
    }


    private JMenu createGameMenu(){
        JMenu gameMenu = new JMenu("Game");

        JMenuItem start = new JMenuItem("Start/Restart");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(null,
                        "Are you sure to start/restart the game?");
                if (option == JOptionPane.YES_OPTION) {
                    new GUI();
                }
            }
        });
        gameMenu.add(start);

        JMenuItem forfeit = new JMenuItem("Forfeit");
        forfeit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(null,
                        "Do you want to stop the game?");
                if (option == JOptionPane.YES_OPTION){
                    JOptionPane.showMessageDialog(null, "OK. Here is your score: "
                            + Game.wScore +
                            "You can start a new game by clicking 'menu -> start/restart'.");
                }

            }
        });
        gameMenu.add(forfeit);

        JMenuItem quit = new JMenuItem("Quit Game");
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(null,
                        "Do you want to exit?");
                if (option == JOptionPane.YES_OPTION){
                    System.exit(0);
                }
            }
        });
        gameMenu.add(quit);

        return gameMenu;
    }


    private JMenu createOperationMenu(){
        JMenu operationMenu = new JMenu("Options");

        JMenuItem undo = new JMenuItem("Undo");
        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        operationMenu.add(undo);


        //PlaceHolder
        JMenuItem redo = new JMenuItem("Redo");
        operationMenu.add(redo);

        return operationMenu;
    }


    private JMenu createScoreMenu(){
        JMenu scoreMenu = new JMenu("Score History");

        JMenuItem display = new JMenuItem("Display score history");
        display.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Score History: B " + Game.bScore + "; W: "+ Game.wScore);
            }
        });
        scoreMenu.add(display);

        return scoreMenu;
    }


    /**
     * This is called in Game after getting player names
     * @param opponentName
     * @param playerName
     */
    public void addPlayerLabel(String opponentName, String playerName){

        JPanel userBoard = new JPanel();
        userBoard.setPreferredSize(USER_BOARD_DIMENSION);

        JLabel opponent = new JLabel(opponentName);
        JLabel player = new JLabel(playerName);

        //By default, player is shown bold in the panel
        Font f = player.getFont();
        player.setFont(f.deriveFont(f.getStyle() | Font.BOLD));

        userBoard.add(opponent);
        userBoard.add(player);

        this.gameFrame.add(userBoard);
        this.gameFrame.setVisible(true);
    }


    /**
     * Board Panel contains default 64 Tile Panels.
     * @author Dorothy
     *
     */
    private class BoardPanel extends JPanel{

        //Default serial versionUID
        private static final long serialVersionUID = 1L;
        private ArrayList<TilePanel> boardTiles;
        private Board board;

        BoardPanel(Board board){
            super(new GridLayout(Board.SIZE, Board.SIZE));
            this.boardTiles = new ArrayList<>();
            this.board = board;

            for (int rowId = 0; rowId < Board.SIZE; rowId ++) {
                for (int colId = 0; colId < Board.SIZE; colId ++) {
                    TilePanel tilePanel = new TilePanel(this.board, rowId, colId);
                    this.boardTiles.add(tilePanel);
                    this.add(tilePanel);
                }
            }

            this.setPreferredSize(BOARD_DIMENSION);
            this.validate();
        }

        public void drawBoard(Board board){
            removeAll();
            for (TilePanel tilePanel: boardTiles){
                tilePanel.drawTile(board);
                this.add(tilePanel);
            }
            validate();
            repaint();
        }
    }


    /**
     * Used by board panel, will change when game starts.
     * @author Dorothy
     *
     */
    private class TilePanel extends JPanel{

        //Default serial versionUID
        private static final long serialVersionUID = 1L;
        private int rowId;
        private int colId;


        TilePanel(Board board, int rowId, int colId){
            super(new GridBagLayout());

            this.rowId = rowId;
            this.colId = colId;

            this.setPreferredSize(TILE_DIMENSION);
            //Add tile color
            this.assignTileColor();
            //Add chess pieces as JLabels
            this.assignTileIcon(board);

            this.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)){ //Right click to un-select a chess piece
                        sourceTile = null;
                    } else if (SwingUtilities.isLeftMouseButton(e)){ //Left click to select a chess piece
                        if (sourceTile == null){
                            sourceTile = board.getPieceAt(rowId, colId);
                        } else {

                            /**
                             * Line 283-288: intends to push a piece
                             * into history stack for undo/redo.
                             */
                            int player = sourceTile.getPlayer();
//                            if (player == 0){ // add to black chess history
//                                historyB.add(sourceTile);
//                            } else { // add to white chess
//                                historyW.add(sourceTile);
//                            }

                            int moveIsSuccessful = board.moveTo(sourceTile, rowId, colId);
                            if (moveIsSuccessful == -1){
                                endTurn = false;

                                /**
                                 * If move fails, gameState won't change
                                 * Remove intended pushing history
                                 */
//                                if (player == 0){
//                                    historyB.remove(historyB.size()-1);
//                                } else {
//                                    historyW.remove(historyW.size()-1);
//                                }

                                JOptionPane.showMessageDialog(null,
                                        "Please choose a legal move using your color piece!");
                            } else if (moveIsSuccessful == 1){
                                endTurn = true;
                                if (board.getKing(1-player) == null){
                                    JOptionPane.showMessageDialog(null, "You win!");
                                }
                            } else {
                                endTurn = true;
                            }
                            sourceTile = null;
                        }
                    }

                    //Update GUI painting
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            boardPanel.drawBoard(board);
                        }
                    });
                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });

            this.validate();
        }


        private void drawTile(Board board){
            assignTileColor();
            assignTileIcon(board);
            validate();
            repaint();
        }


        /**
         * According to a standard chess board, color of each tile alternates conseutively.
         */
        private void assignTileColor() {
            if (this.rowId % 2 == 0) {
                if (this.colId % 2 == 0) {
                    this.setBackground(light_tile_color);
                } else {
                    this.setBackground(dark_tile_color);
                }
            } else {
                if (this.colId % 2 == 0) {
                    this.setBackground(dark_tile_color);
                } else {
                    this.setBackground(light_tile_color);
                }
            }
        }


        /**
         * Initialize chess piece on the board display.
         */
        private void assignTileIcon(Board board) {

            this.removeAll();
            ChessPiece piece = board.getPieceAt(this.rowId, this.colId);
            int player;
            char type;

            if(piece != null) {
                player = piece.getPlayer();
                type = piece.getType();
                try{
                    final BufferedImage image = ImageIO.read(new File(BASE_PATH +
                            player + type + ".png"));
                    add(new JLabel(new ImageIcon(image)));
                } catch(final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}