package controller;

import model.chessBoard.*;
import view.graphGUI.GUI;

import javax.swing.*;


public class Game {

    public Board board;
    public GUI table;
    public int turn;
    public static int wScore = 0; //White chess score history
    public static int bScore = 0; //Black chess score history
    public boolean endGame = false;
    public String player;


    public void gameLoop(){

        //Start a new game
        this.table = new GUI();
        this.board = new Board();
        this.player = "";

        this.wScore = 0;
        this.bScore = 0;
        this.turn = this.board.turn;

        String opponentName = JOptionPane.showInputDialog(null, "Black chess name: ");
        String playerName = JOptionPane.showInputDialog(null, "White chess name: ");

        this.table.addPlayerLabel(opponentName, playerName);

        while (!endGame){

            /**
             * If player1 intends to move one piece,
             * check: if player1 is inCheck, or is checkmated
             * check: if the piece belongs to player1
             * check: if the move is legal, then move and update GUI & movement stack
             * check: if the opponent king is killed, then endGame
             * Update turn
             */

            if (this.board.isInCheck(this.turn)){
                JOptionPane.showMessageDialog(null, "You're in check!");
            }
            if (this.board.checkmate() != -1){
                JOptionPane.showMessageDialog(null, "Checkmate reminder!");
            }
            if (this.board.getKing(this.turn).isCaptured()){

                //Opponent player wins
                //record score history
                endGame = true;
                if (this.turn == 0){
                    this.wScore += 1;
                    this.player = "White chess";
                } else {
                    this.bScore += 1;
                    this.player = "Black chess";
                }
                JOptionPane.showMessageDialog(null,
                        "Congratulations! "+this.player+" wins!");
            } else if (this.board.getKing((this.turn+1)%2).isCaptured()){

                //Turn player wins
                //record score history
                endGame = true;
                if (this.turn == 0){
                    this.bScore += 1;
                    this.player = "Black chess";
                } else {
                    this.wScore += 1;
                    this.player = "White chess";
                }
                JOptionPane.showMessageDialog(null,
                        "Congratulations! "+this.player+" wins!");
            }

            if (!GUI.endTurn){
                try {
                   Thread.sleep(500);
                } catch (InterruptedException e){
                }
            } else {
                GUI.endTurn = false;
                this.turn = 1-this.turn;
            }
        }
    }

    public static void main(String[] args) {
        new Game().gameLoop();
    }
}
